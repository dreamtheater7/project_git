# !/usr/bin/env python3
# -*-coding:cp1252-*-
# this is a greatest common devisor program.

def main():
    print("this is a mkd program.")

    # recursion
    def mkd(a,b):
        if a==0:
            return b
        elif b==0:
            return a
        return mkd(b, a%b)

    
if __name__ == "__main__":
    main()

